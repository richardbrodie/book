#include <zip.h>
#include <fstream>
#include <iostream>
#include <optional>
#include <pugixml.hpp>
#include "books.hpp"

std::optional<std::string> open_entry(zip_t *zip)
{
    const char *entry = "META-INF/container.xml";
    auto loc = zip_name_locate(zip, entry, 0);
    if (loc >= 0) {
        struct zip_stat zs;
        zip_stat_index(zip, loc, 0, &zs);
        char *contents = new char[zs.size];
        zip_file *f = zip_fopen_index(zip, loc, 0);
        zip_fread(f, contents, zs.size);
        pugi::xml_document doc;
        pugi::xml_parse_result result = doc.load_string(contents);

        pugi::xml_node metadata =
            doc.select_node("//container/rootfiles/rootfile").node();
        return metadata.attribute("full-path").value();
    }
    return {};
}

int main()
{
    int err = 0;
    zip_t *z = zip_open("book.epub", ZIP_RDONLY, &err);
    auto a = open_entry(z);
    if (!a) {
        return 1;
    }
    // auto num_entries = zip_get_num_entries(z, 0);
    // for (auto i = 0; i < num_entries; i++) {
    //    auto name = zip_get_name(z, i, 0);
    //    struct zip_stat zs;
    //    zip_stat(z, name, 0, &zs);
    //    dbg(name);
    //}

    auto name = a.value();
    struct zip_stat zs;
    zip_stat(z, name.c_str(), 0, &zs);
    // Alloc memory for its uncompressed contents
    char *contents = new char[zs.size];

    // Read the compressed file
    zip_file *f = zip_fopen(z, name.c_str(), 0);
    zip_fread(f, contents, zs.size);
    zip_fclose(f);
    //
    //// And close the archive
    // zip_close(z);
    //
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load_string(contents);

    pugi::xml_node metadata = doc.select_node("//package/metadata").node();

    dbg("Title", metadata.child_value("dc:title"));
    dbg("Author", metadata.child("dc:creator").child_value());
    dbg(metadata.child("dc:creator").attribute("opf:file-as").value());

    std::cout << doc.child("package").child("metadata").child_value("dc:title")
              << std::endl;

    return 0;
}
