#include "books.hpp"
#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <pugixml.hpp>
#include <zip.h>

class Author {
public:
  std::string name;
  std::string sort_name;

  Author(const char *n, const char *sn)
      : name(std::string(n)), sort_name(std::string(sn)) {}
};

class EpubMetadata {
public:
  std::string title;
  Author author;

  EpubMetadata(const char *t, Author a) : title(std::string(t)), author(a) {}
};

std::optional<char *> open_zip_file(const char *entry, zip_t *zip) {
  auto loc = zip_name_locate(zip, entry, 0);
  if (loc <= 0) {
    return {};
  }

  struct zip_stat zs;
  zip_stat_index(zip, loc, 0, &zs);
  char *contents = new char[zs.size];
  zip_file *f = zip_fopen_index(zip, loc, 0);
  zip_fread(f, contents, zs.size);
  zip_fclose(f);
  return contents;
}

std::optional<char *> locate_opf(zip_t *zip) {
  const char *entry = "META-INF/container.xml";
  auto op_c = open_zip_file(entry, zip);
  if (!op_c)
    return {};

  auto contents = op_c.value();
  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_string(contents);

  pugi::xml_node metadata =
      doc.select_node("//container/rootfiles/rootfile").node();
  auto opf_name = metadata.attribute("full-path").value();
  auto opf_contents = open_zip_file(opf_name, zip);
  return opf_contents;
}

zip_t *open_zip(const char *path) {
  int err = 0;
  zip_t *z = zip_open(path, ZIP_RDONLY, &err);
  // if (err)
  //   return 1;

  return z;
}

void open_epub(const char *path) {
  if (!std::filesystem::exists(path))
    return;
  zip_t *z = open_zip(path);

  auto opf = locate_opf(z);
  if (!opf)
    return;
  auto opf_data = opf.value();

  zip_close(z);

  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_string(opf_data);

  pugi::xml_node metadata = doc.select_node("//package/metadata").node();
  auto author_child = metadata.child("dc:creator");

  Author author(author_child.child_value(),
                author_child.attribute("opf:file-as").value());

  auto title = metadata.child_value("dc:title");
  EpubMetadata meta(title, author);

  dbg(meta.title);
  dbg(meta.author.name);
}

int main() {
  std::string books[2] = {"book.epub", "book2.epub"};

  for (auto b = std::begin(books); b != std::end(books); ++b) {
    open_epub(b->c_str());
  }
  // for (auto b = std::begin(books); b != std::end(books); ++b) {
  //   int err = 0;
  //   zip_t *z = zip_open("book.epub", ZIP_RDONLY, &err);
  // }

  // int err = 0;
  // zip_t *z = zip_open("book.epub", ZIP_RDONLY, &err);
  // if (err)
  //   return 1;

  // auto opf = locate_opf(z);
  // if (!opf)
  //   return 1;
  // auto opf_data = opf.value();

  // pugi::xml_document doc;
  // pugi::xml_parse_result result = doc.load_string(opf_data);

  // pugi::xml_node metadata = doc.select_node("//package/metadata").node();
  // auto author_child = metadata.child("dc:creator");

  // Author author(author_child.child_value(),
  //               author_child.attribute("opf:file-as").value());

  // auto title = metadata.child_value("dc:title");
  // EpubMetadata meta(title, author);

  // dbg(meta.title);
  // dbg(meta.author.name);
  return 0;
}
